import * as jose from 'jose';


export const decodeJwt = async (token: string) => {
  try {
    const enc = new TextEncoder();
    const secret = enc.encode('slime@royale2xw=s*g)h$k%+-jjd(2bu!d(5r%-7if4)1ffqjhqcrop');
    const { payload } = await jose.jwtVerify(token, secret);
    return payload;
  } catch (e) {
    console.log(e)
  }
};

export const encodeJwt = async (data: {}) => {
  const enc = new TextEncoder();
  const secret = enc.encode('slime@royale2xw=s*g)h$k%+-jjd(2bu!d(5r%-7if4)1ffqjhqcrop');
  return await new jose.SignJWT(data).setProtectedHeader({ alg: 'HS256' }).sign(secret);
};

export const decodeUser = async (token:string) => {
  console.log(token)
  try {
    const enc = new TextEncoder();
    const secret = enc.encode('thisisasamplesecret');
    const { payload } = await jose.jwtVerify(token, secret);
    return payload;
  } catch (e) {
    console.log(e)
  }
};
export const setData = async (key:any, value:any) => {
  await localStorage.setItem(key, JSON.stringify(value));
}

export const authDataStorage = (tokens:any) => {
  return new Promise(async (resolve) => {
    try {
      const { refresh, access } = tokens;
      if(!refresh) resolve(false);
      await setData('accessToken', access.token);
      await setData('refreshToken', refresh.token);
      resolve(true);
    } catch(err) {
      resolve(false);
    }
  })
}