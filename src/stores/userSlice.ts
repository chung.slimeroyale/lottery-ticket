import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '.'

export interface CounterState {
  role: String,
  isEmailVerified: boolean,
  email: String,
  name: String,
  id: String
}

const initialState: CounterState = {
  role: "",
  isEmailVerified: false,
  email: "",
  name: "",
  id: ""

}

export const counterSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setAccountData: (state, action: PayloadAction<any>) => {
      console.log(action)
      state = {
        ...state,
        ...action.payload,
      };
      return state
    },
  },
})

// Action creators are generated for each case reducer function
export const {setAccountData} = counterSlice.actions

export default counterSlice.reducer
