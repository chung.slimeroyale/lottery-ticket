import axios from "axios";

const instance = axios.create({
  baseURL:  "https://slimeroyale.com/api",
  // baseURL:  "http://localhost:4000/api",
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  }
})

const TOKEN_PAYLOAD_KEY = 'Authorization'

instance.interceptors.request.use(async (config) => {
  const tokens = JSON.parse(localStorage.getItem('accessToken') as any);
  if (tokens) {
    if (config.headers) {
      config.headers[TOKEN_PAYLOAD_KEY] = `Bearer ${tokens}`
    }
  }
  return config
})

export default instance;
