import instance from "../instance";

export const login = (data:any)=>{
  let url=`/auth/login`;
  return instance.post(url,data)
}
export const refreshToken = (data:any)=>{
  let url=`/auth/refresh-tokens`;
  return instance.post(url,data)
}

export const getEvents = () =>{
  let url=`/events`;
  return instance.get(url)
}
export const addNumberLottery = (data:any) =>{
  let url=`/lotteries/add`;
  return instance.post(url,data)
}
export const getEventId = (id:any) =>{
  let url=`/events/${id}`;
  return instance.get(url)
}
export const getPrizes = () =>{
  let url=`/prizes`;
  return instance.get(url)
}
export const getPrize = (id:string) =>{
  let url=`/prizes/${id}`;
  return instance.get(url)
}