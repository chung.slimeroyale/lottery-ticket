import React from "react";
import {Button, Col, Modal, Row} from "react-bootstrap";
import {addNumberLottery} from "../service/api-service/auth";

interface ModalConfirmProps {
  show: boolean;
  onClose: () => void;
  event: any;
  luckyNumber: any;
  prize:string
}

const ModalConfirm: React.FunctionComponent<ModalConfirmProps> = ({show, onClose, event, luckyNumber,prize}) => {

  const handleConfirm = async () =>{
    onClose()
    await addNumberLottery({
      event: event.id,
      number: luckyNumber,
      prize:prize
    }).then(res => {
      console.log(res);
      onClose()
    }).catch(err => {
      console.log(err)
    })
  }

  return (
    <Modal
      show={show}
      size="lg"
      centered
      className={"modal-spin-success animate__animated animate__tada"}
    >
      <Modal.Body>
        <div style={{position: "relative"}}>
          <div className={"d-flex justify-content-center"}>
            <img className={"starTicket"} src={require("../assets/star.png")} alt=""/>
          </div>
          <div style={{position: "relative", zIndex: 10}}>
            <div className={"text-center mt-4"}>
              <h4 style={{color: "#FFFFFF", fontWeight: 700}}>
                Congratulations! Winning lottery ticket
              </h4>
              <h1 className={"text-gradient-pig"}>{event?.name}</h1>
            </div>
            <Row style={{width: ""}} className={"d-flex justify-content-center mt-5"}>
              {luckyNumber.map((item: any) => {
                return (
                  <Col xl={3}>
                    <div className={"ticketBox"}>
                      {item}
                    </div>
                  </Col>
                )
              })}

            </Row>
            <h5 className={"text-center text-light mt-3 notice"}>
              If you win, please visit our website to confirm your prize.
            </h5>
            <div className={"mt-5 d-flex justify-content-center"}>
              <Button onClick={handleConfirm} className={"btnConfirm"}>CONFIRM</Button>
            </div>
          </div>
        </div>
      </Modal.Body>

    </Modal>
  )
}
export default ModalConfirm