import React, {useEffect, useState} from 'react';
import useLoadUserData from "../LoadUserData";
import {useNavigate} from "react-router-dom";

const LoadData = ({children}:any) => {
  const navigate = useNavigate()
  const {user, loading} = useLoadUserData();
  useEffect(() => {
    authentication();
  }, [user, loading]);
 const authentication = async () =>{
   if (!loading && user === null) {
       await navigate('/login',{replace:true});
   }
 }
  return (<>
    {loading ? <div>

    </div> : children}
  </>);
}
export default LoadData;
